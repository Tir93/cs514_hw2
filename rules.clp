(import nrc.fuzzy.jess.*)
(import nrc.fuzzy.*)

;-----------------------------------------------------------------------------------------
;Fuzzy Variables
;-----------------------------------------------------------------------------------------
(defglobal ?*PositionX* = (new FuzzyVariable "positionX" -250 250 "m"))
(defglobal ?*VelocityX* = (new FuzzyVariable "VelocityX" -50 50 "m/s"))
(defglobal ?*VelocityY* = (new FuzzyVariable "VelocityY" 0 100 "m/s"))
(defglobal ?*WindDerivativeX* = (new FuzzyVariable "WindDerivativeX" -10 10 "N/s"))
(defglobal ?*WindDerivativeY* = (new FuzzyVariable "WindDerivativeY" -10 10 "N/s"))
(defglobal ?*ForceX* = (new FuzzyVariable "ForceX" -20 20 "N"))
(defglobal ?*ForceY* = (new FuzzyVariable "ForceY" -20 20 "N"))

;-----------------------------------------------------------------------------------------
;Fuzzy Sets
;-----------------------------------------------------------------------------------------
(defrule init 
    (declare (salience 100))
    =>
    (load-package FuzzyFunctions) 
	;PositionX
	(bind ?xZ  (create$ -30 0 30)) 
	(bind ?yZ  (create$ 0 1 0))
	(?*PositionX* addTerm "Z" ?xZ ?yZ 3)
	(bind ?xP  (create$ 0 30 60)) 
	(bind ?yP  (create$ 0 1 0))
	(?*PositionX* addTerm "P" ?xP ?yP 3)
	(bind ?xN  (create$ -60 -30 0)) 
	(bind ?yN  (create$ 0 1 0))
	(?*PositionX* addTerm "N" ?xN ?yN 3)
	(bind ?xVP  (create$ 30 60)) 
	(bind ?yVP  (create$ 0 1))
	(?*PositionX* addTerm "VP" ?xVP ?yVP 2)
	(bind ?xVN  (create$ -60 -30)) 
	(bind ?yVN  (create$ 1 0))
	(?*PositionX* addTerm "VN" ?xVN ?yVN 2)

	(bind ?vals (create$ (new FuzzyValue ?*PositionX* "Z") (new FuzzyValue ?*PositionX* "P") (new FuzzyValue ?*PositionX* "N") (new FuzzyValue ?*PositionX* "VP") (new FuzzyValue ?*PositionX* "VN"))) 
	(printout t (call FuzzyValue plotFuzzyValues "*+o--" ?vals) crlf) 
 
	;VelocityX
	(bind ?xZ  (create$ -20 0 20)) 
	(bind ?yZ  (create$ 0 1 0))
	(?*VelocityX* addTerm "Z" ?xZ ?yZ 3)
	(bind ?xP  (create$ 0 20)) 
	(bind ?yP  (create$ 0 1))
	(?*VelocityX* addTerm "P" ?xP ?yP 2)
	(bind ?xN  (create$ -20  0)) 
	(bind ?yN  (create$ 1 0))
	(?*VelocityX* addTerm "N" ?xN ?yN 2)
	
	(bind ?vals (create$ (new FuzzyValue ?*VelocityX* "Z") (new FuzzyValue ?*VelocityX* "P") (new FuzzyValue ?*VelocityX* "N"))) 
	(printout t (call FuzzyValue plotFuzzyValues "*+o--" ?vals) crlf)

	;VelocityY
	(bind ?xT  (create$ 40 50 60)) 
	(bind ?yT  (create$ 0 1 0))
	(?*VelocityY* addTerm "T" ?xT ?yT 3)
	(bind ?xH  (create$ 50 60 70)) 
	(bind ?yH  (create$ 0 1 0))
	(?*VelocityY* addTerm "H" ?xH ?yH 3)
	(bind ?xL  (create$ 30 40 50)) 
	(bind ?yL  (create$ 0 1 0))
	(?*VelocityY* addTerm "L" ?xL ?yL 3)
	(bind ?xVH  (create$ 60 70)) 
	(bind ?yVH  (create$ 0 1))
	(?*VelocityY* addTerm "VH" ?xVH ?yVH 2)
	(bind ?xVL  (create$ 30 40)) 
	(bind ?yVL  (create$ 1 0))
	(?*VelocityY* addTerm "VL" ?xVL ?yVL 2)
	
	(bind ?vals (create$ (new FuzzyValue ?*VelocityY* "T") (new FuzzyValue ?*VelocityY* "H") (new FuzzyValue ?*VelocityY* "L") (new FuzzyValue ?*VelocityY* "VH") (new FuzzyValue ?*VelocityY* "VL"))) 
	(printout t (call FuzzyValue plotFuzzyValues "*+o--" ?vals) crlf)
	
	;WindDerivativeX
	(bind ?xZ  (create$ -5 0 5)) 
	(bind ?yZ  (create$ 0 1 0))
	(?*WindDerivativeX* addTerm "Z" ?xZ ?yZ 3)
	(bind ?xP  (create$ 0 5)) 
	(bind ?yP  (create$ 0 1))
	(?*WindDerivativeX* addTerm "P" ?xP ?yP 2)
	(bind ?xN  (create$ -5  0)) 
	(bind ?yN  (create$ 1 0))
	(?*WindDerivativeX* addTerm "N" ?xN ?yN 2)
	
	(bind ?vals (create$ (new FuzzyValue ?*WindDerivativeX* "Z") (new FuzzyValue ?*WindDerivativeX* "P") (new FuzzyValue ?*WindDerivativeX* "N"))) 
	(printout t (call FuzzyValue plotFuzzyValues "*+o--" ?vals) crlf)

	;WindDerivativeY
	(bind ?xZ  (create$ -5 0 5)) 
	(bind ?yZ  (create$ 0 1 0))
	(?*WindDerivativeY* addTerm "Z" ?xZ ?yZ 3)
	(bind ?xP  (create$ 0 5)) 
	(bind ?yP  (create$ 0 1))
	(?*WindDerivativeY* addTerm "P" ?xP ?yP 2)
	(bind ?xN  (create$ -5  0)) 
	(bind ?yN  (create$ 1 0))
	(?*WindDerivativeY* addTerm "N" ?xN ?yN 2)
	
	(bind ?vals (create$ (new FuzzyValue ?*WindDerivativeY* "Z") (new FuzzyValue ?*WindDerivativeY* "P") (new FuzzyValue ?*WindDerivativeY* "N"))) 
	(printout t (call FuzzyValue plotFuzzyValues "*+o--" ?vals) crlf)
	
	;ForceX
	(bind ?xZ  (create$ -5 0 5)) 
	(bind ?yZ  (create$ 0 1 0))
	(?*ForceX* addTerm "Z" ?xZ ?yZ 3)
	(bind ?xP  (create$ 0 5 10)) 
	(bind ?yP  (create$ 0 1 0))
	(?*ForceX* addTerm "P" ?xP ?yP 3)
	(bind ?xN  (create$ -10 -5 0)) 
	(bind ?yN  (create$ 0 1 0))
	(?*ForceX* addTerm "N" ?xN ?yN 3)
	(bind ?xVP  (create$ 5 10)) 
	(bind ?yVP  (create$ 0 1))
	(?*ForceX* addTerm "VP" ?xVP ?yVP 2)
	(bind ?xVN  (create$ -10 -5)) 
	(bind ?yVN  (create$ 1 0))
	(?*ForceX* addTerm "VN" ?xVN ?yVN 2)
	
	(bind ?vals (create$ (new FuzzyValue ?*ForceX* "Z") (new FuzzyValue ?*ForceX* "P") (new FuzzyValue ?*ForceX* "N") (new FuzzyValue ?*ForceX* "VP") (new FuzzyValue ?*ForceX* "VN"))) 
	(printout t (call FuzzyValue plotFuzzyValues "*+o--" ?vals) crlf)

	;ForceY
	(bind ?xZ  (create$ -5 0 5)) 
	(bind ?yZ  (create$ 0 1 0))
	(?*ForceY* addTerm "Z" ?xZ ?yZ 3)
	(bind ?xP  (create$ 0 5 10)) 
	(bind ?yP  (create$ 0 1 0))
	(?*ForceY* addTerm "P" ?xP ?yP 3)
	(bind ?xN  (create$ -10 -5 0)) 
	(bind ?yN  (create$ 0 1 0))
	(?*ForceY* addTerm "N" ?xN ?yN 3)
	(bind ?xVP  (create$ 5 10)) 
	(bind ?yVP  (create$ 0 1))
	(?*ForceY* addTerm "VP" ?xVP ?yVP 2)
	(bind ?xVN  (create$ -10 -5)) 
	(bind ?yVN  (create$ 1 0))
	(?*ForceY* addTerm "VN" ?xVN ?yVN 2)
	
	(bind ?vals (create$ (new FuzzyValue ?*ForceY* "Z") (new FuzzyValue ?*ForceY* "P") (new FuzzyValue ?*ForceY* "N") (new FuzzyValue ?*ForceY* "VP") (new FuzzyValue ?*ForceY* "VN"))) 
	(printout t (call FuzzyValue plotFuzzyValues "*+o--" ?vals) crlf)
    
    (call FuzzyValue setConfineFuzzySetsToUOD TRUE)
)

;-----------------------------------------------------------------------------------------
;Templates
;-----------------------------------------------------------------------------------------
(deftemplate measuredPositionX (slot value (type FLOAT)))
(deftemplate measuredVelocityX (slot value (type FLOAT)))
(deftemplate measuredVelocityY (slot value (type FLOAT)))
(deftemplate measuredWindDerivativeX (slot value (type FLOAT)))
(deftemplate measuredWindDerivativeY (slot value (type FLOAT)))
(deftemplate crispForceX (slot value (type FLOAT)))
(deftemplate crispForceY (slot value (type FLOAT)))
;-----------------------------------------------------------------------------------------
;Functions
;-----------------------------------------------------------------------------------------
(deffunction assertPosX (?v) (return (assert (measuredPositionX (value ?v)))))
(deffunction assertVelX (?v) (return (assert (measuredVelocityX (value ?v)))))
(deffunction assertVelY (?v) (return (assert (measuredVelocityY (value ?v)))))
(deffunction assertDerX (?v) (return (assert (measuredWindDerivativeX (value ?v)))))
(deffunction assertDerY (?v) (return (assert (measuredWindDerivativeY (value ?v)))))
;-----------------------------------------------------------------------------------------
;Fuzzyfication
;-----------------------------------------------------------------------------------------
(defrule fuzzifyPositionX
  (declare (salience 50))
  (measuredPositionX (value ?x))
  => (assert (fuzzyPositionX (new FuzzyValue ?*PositionX* (new GaussianFuzzySet ?x 5))))
)
(defrule fuzzifyVelocityX
  (declare (salience 50))
  (measuredVelocityX (value ?x))
  => (assert (fuzzyVelocityX (new FuzzyValue ?*VelocityX* (new GaussianFuzzySet ?x 5))))
)
(defrule fuzzifyVelocityY
  (declare (salience 50))
  (measuredVelocityY (value ?x))
  => (assert (fuzzyVelocityY (new FuzzyValue ?*VelocityY* (new GaussianFuzzySet ?x 5))))
)
(defrule fuzzifyWindDerivativeX
  (declare (salience 50))
  (measuredWindDerivativeX (value ?x))
  => (assert (fuzzyWindDerivativeX (new FuzzyValue ?*WindDerivativeX* (new GaussianFuzzySet ?x 5))))
)
(defrule fuzzifyWindDerivativeY
  (declare (salience 50))
  (measuredWindDerivativeY (value ?x))
  => (assert (fuzzyWindDerivativeY (new FuzzyValue ?*WindDerivativeY* (new GaussianFuzzySet ?x 5))))
)
;-----------------------------------------------------------------------------------------
;Fuzzyfication
;-----------------------------------------------------------------------------------------
(defrule defuzzify
    (declare (salience -10))
    (fuzzyForceX ?fx)
    (fuzzyForceY ?fy)
    =>
    (assert (crispForceX (value (?fx momentDefuzzify))))
   	(assert (crispForceY (value (?fy momentDefuzzify))))
)
;-----------------------------------------------------------------------------------------
;Queries
;-----------------------------------------------------------------------------------------
(defquery getForceX (crispForceX (value ?v)))
(defquery getForceY (crispForceY (value ?v)))
;-----------------------------------------------------------------------------------------
;Rules
;-----------------------------------------------------------------------------------------
;SpeedControl
(defrule rule1
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "T"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "N")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "P"))) 
)

(defrule rule2
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "H"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "N")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "Z"))) 
)

(defrule rule3
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "L"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "N")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "VP"))) 
)

(defrule rule4
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "VH"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "N")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "N"))) 
)

(defrule rule5
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "VL"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "N")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "VP"))) 
)

(defrule rule6
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "T"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "P")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "N"))) 
)

(defrule rule7
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "H"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "P")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "VN"))) 
)

(defrule rule8
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "L"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "P")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "Z"))) 
)

(defrule rule9
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "VH"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "P"))  
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "VN"))) 
)

(defrule rule10
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "VL"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "P")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "P"))) 
)

(defrule rule11
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "T"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "Z")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "Z"))) 
)

(defrule rule12
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "H"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "Z"))
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "N"))) 
)

(defrule rule13
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "L"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "Z"))
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "P"))) 
)

(defrule rule14
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "VH"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "Z")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "VN"))) 
)

(defrule rule15
   (fuzzyVelocityY ?t&:(fuzzy-match ?t "VL"))
   (fuzzyWindDerivativeY ?w&:(fuzzy-match ?w "Z")) 
   => 
   (assert (fuzzyForceY (new nrc.fuzzy.FuzzyValue ?*ForceY* "VP"))) 
)

;CenterControl
(defrule rule16a
   (fuzzyPositionX ?p&:(fuzzy-match ?p "Z"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "N"))
   (fuzzyVelocityX ?v&:(fuzzy-match ?v "not P"))
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "P"))) 
)

(defrule rule16b
   (fuzzyPositionX ?p&:(fuzzy-match ?p "Z"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "N"))
   (fuzzyVelocityX ?v&:(fuzzy-match ?v "P"))
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "Z"))) 
)

(defrule rule17
   (fuzzyPositionX ?p&:(fuzzy-match ?p "P"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "N")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "Z"))) 
)

(defrule rule18
   (fuzzyPositionX ?p&:(fuzzy-match ?p "N"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "N")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "VP"))) 
)

(defrule rule19
   (fuzzyPositionX ?p&:(fuzzy-match ?p "VP"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "N")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "N"))) 
)

(defrule rule20
   (fuzzyPositionX ?p&:(fuzzy-match ?p "VN"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "N")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "VP"))) 
)

(defrule rule21a
   (fuzzyPositionX ?p&:(fuzzy-match ?p "Z"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "P"))
   (fuzzyVelocityX ?v&:(fuzzy-match ?v "not N"))
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "N"))) 
)

(defrule rule21b
   (fuzzyPositionX ?p&:(fuzzy-match ?p "Z"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "P"))
   (fuzzyVelocityX ?v&:(fuzzy-match ?v "N"))
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "Z"))) 
)

(defrule rule22
   (fuzzyPositionX ?p&:(fuzzy-match ?p "P"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "P")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "VN"))) 
)

(defrule rule23
   (fuzzyPositionX ?p&:(fuzzy-match ?p "N"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "P")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "Z"))) 
)

(defrule rule24
   (fuzzyPositionX ?p&:(fuzzy-match ?p "VP"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "P")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "VN"))) 
)

(defrule rule25
   (fuzzyPositionX ?p&:(fuzzy-match ?p "VN"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "P")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "P"))) 
)

(defrule rule26a
   (fuzzyPositionX ?p&:(fuzzy-match ?p "Z"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "Z")) 
   (fuzzyVelocityX ?v&:(fuzzy-match ?v "Z"))
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "Z")))
)

(defrule rule26b
   (fuzzyPositionX ?p&:(fuzzy-match ?p "Z"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "Z")) 
   (fuzzyVelocityX ?v&:(fuzzy-match ?v "P"))
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "N")))
)

(defrule rule26c
   (fuzzyPositionX ?p&:(fuzzy-match ?p "Z"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "Z")) 
   (fuzzyVelocityX ?v&:(fuzzy-match ?v "N"))
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "P")))
)

(defrule rule27
   (fuzzyPositionX ?p&:(fuzzy-match ?p "P"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "Z")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "N")))
)

(defrule rule28
   (fuzzyPositionX ?p&:(fuzzy-match ?p "N"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "Z")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "P")))
)

(defrule rule29
   (fuzzyPositionX ?p&:(fuzzy-match ?p "VP"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "Z")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "VN")))
)

(defrule rule30
   (fuzzyPositionX ?p&:(fuzzy-match ?p "VN"))
   (fuzzyWindDerivativeX ?w&:(fuzzy-match ?w "Z")) 
   => 
   (assert (fuzzyForceX (new nrc.fuzzy.FuzzyValue ?*ForceX* "VP")))
)

(defrule print
   (declare (salience -20))
   (fuzzyForceX ?fx)
   (fuzzyForceY ?fy) 
   (crispForceX (value ?cfx))
   (crispForceY (value ?cfy))
   =>
   (printout t crlf "-----------------------------------------------------------------------" crlf "Defuzzified ForceX = " ?cfx crlf "-----------------------------------------------------------------------" crlf)
   (printout t (?fx plotFuzzyValue "*") crlf)
   (printout t crlf "-----------------------------------------------------------------------" crlf "Defuzzified ForceY = " ?cfy crlf "-----------------------------------------------------------------------" crlf)
   (printout t (?fy plotFuzzyValue "*") crlf)
   (printout t crlf "-----------------------------------------------------------------------" crlf)  
)
