------------------------------------------------------------------------------
IMPORTANT NOTICE
------------------------------------------------------------------------------
I could not include the .jar file into the email due to security reasons.
You will find it in the following git repository:
	https://Tir93@bitbucket.org/Tir93/cs514_hw2.git
------------------------------------------------------------------------------
HOW TO RUN THE SYSTEM
------------------------------------------------------------------------------
I suppose FuzzyJess (and Java) is already configured on your system. In particular, you 
should have already add to your CLASSPATH the archives jess.jar and 
fuzzyJ-OpenSource-2.0.jar. If not, you have to add them manually. On Linux,
add the following lines to your .bashrc file:
	CLASSPATH=$CLASSPATH:<path-to-your-rar>/fuzzyJ-OpenSource-2.0.jar
	CLASSPATH=$CLASSPATH:<path-to-you-jess-folder>/lib/jess.jar
Then, restart your shell by logging out and back in, or:
	source .bashrc
Once you have done this, simply run the system by typing:
	java nrc.fuzzy.jess.FuzzyMain system.clp
The system loads external files using a relative path. This means that it will
look for files in your CURRENT folder (the one from which you run Jess script).
Thus, you must run the system from the folder where all files are located.
Example (in Linux): if the folder with all the .clp files is in
	/home/user/HW1
you have to move into the HW1 directory:
	cd /home/user/HW1
and, from there, run FUZZY jess (not the normal jess) by passing system.clp:
	java nrc.fuzzy.jess.FuzzyMain system.clp
Extension to Windows and Mac is higly trivial. If not, please contact me and
I will provide a small guide.

If you want to run the java simulator, simply move to the directory with all
the files and type:
	java -jar hw2.jar
The program needs some arguments. If you do not specify them, you will get
a help display that specifies what to include. An example of run with arguments
is:
	java -jar hw2.jar 30 -wind 4 4 -noise 0 -position 0 0
------------------------------------------------------------------------------
NOTES
------------------------------------------------------------------------------
The application was developed and tested with Jess 7 and Java 7 on Linux.
You will find a detailed description in hw2.pdf.
------------------------------------------------------------------------------