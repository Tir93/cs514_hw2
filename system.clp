;(watch all)

;The entry point of our system
(printout t crlf "-----------------------------------------------------------------------" crlf "HW2 Expert System : Autonomous Car Control" crlf "-----------------------------------------------------------------------" crlf)

;Import FuzzyJ libraries
(import nrc.fuzzy.jess.*)
(import nrc.fuzzy.*)

;We loop until the user decides to exit
(while (= 1 1) 
    (printout t crlf "-----------------------------------------------------------------------" crlf crlf)
    (reset)
  	(printout t "Loading Rules..." crlf)
    (batch "rules.clp")
    (printout t "Rules loaded successfully." crlf crlf)
    (printout t "What would you like to do?" crlf)
    (printout t "0 - EXIT" crlf)
    (printout t "1 - RUN INFERENCE" crlf)
    ;Read user choice
    (bind ?choice (readline))
    ;If choice is 0, we exit
    (if (= ?choice "0") 
        then (printout t "Goodbye." crlf) (break) 
        ;If choice is 2, we start the interactive data collection
        elif (= ?choice "1") then 
        	;Collect measurements
        	(printout t "Insert a value for the X position (from -100 to 100).")
        	(bind ?posX (call Double parseDouble (readline)))
        	(if (or (< ?posX -100) (> ?posX 100)) then (printout t "ERROR" crlf) (continue))
            (printout t "Insert a value for the X velocity (from -50 to 50).")
        	(bind ?velX (call Double parseDouble (readline)))
        	(if (or (< ?velX -50) (> ?velX 50)) then (printout t "ERROR" crlf) (continue))
            (printout t "Insert a value for the Y velocity (from 0 to 100).")
        	(bind ?velY (call Double parseDouble (readline)))
        	(if (or (< ?velY 0) (> ?velY 100)) then (printout t "ERROR" crlf) (continue))
            (printout t "Insert a value for the X derivative of the wind force (from -10 to 10).")
        	(bind ?derX (call Double parseDouble (readline)))
        	(if (or (< ?derX -10) (> ?derX 10)) then (printout t "ERROR" crlf) (continue))
            (printout t "Insert a value for the Y derivative of the wind force (from -10 to 10).")
        	(bind ?derY (call Double parseDouble (readline)))
        	(if (or (< ?derY -10) (> ?derY 10)) then (printout t "ERROR" crlf) (continue))
        	;Assert the crisp values
        	(assertPosX ?posX)
        	(assertVelX ?velX)
        	(assertVelY ?velY)
        	(assertDerX ?derX)
        	(assertDerY ?derY)
        	;Run the system
			(run)
			;(facts)
        ;If choice is neither 0, nor 1, nor 2, we return an error
        else (printout t "You must insert either 0 or 1." crlf) (continue)))